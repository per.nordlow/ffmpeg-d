module av;

// NOTE non-public imports needs to be here; otherwise DCD cannot find definitions symbols
import av.libavformat.avformat;
import av.libavformat.avio;
import av.libavformat.avformat_version;
import av.libavcodec.avcodec;
import av.libavcodec.avcodec_version;
import av.libavutil.avutil;
import av.libavutil.frame;
import av.libavutil.pixdesc;
import av.libavutil.imgutils;
import av.libavutil.samplefmt;
import av.libavdevice.avdevice;
import av.libavdevice.avdevice_version;
import av.libavfilter.avfilter;
import av.libavfilter.avfilter_version;

version(unittest)
{
    import backtrace.backtrace;
    import std.stdio;
}

// Installed on Ubuntu using: sudo apt-get install libavformat-dev libavcodec-dev libavfilter-dev
pragma(lib, "avformat");
pragma(lib, "avcodec");
pragma(lib, "avfilter");
pragma(lib, "avutil");

import std.string: toStringz;
import std.range.primitives: empty, ElementType;
import std.conv: to;
import std.stdio: write, writeln, writefln, writef;
import std.file: FileException;
import std.typecons: Tuple, tuple, RefCounted;

shared static this() // TODO remove shared?
{
    av_register_all();
}

void copyVideo(const AVFrame* source,
               AVFrame* target)
{
    if (source is target) return;

    if (target.width < source.width)   { throw new Exception("target width smaller than source width"); }
    if (target.height < source.height) { throw new Exception("target height smaller than source height"); }

    const format = target.format.to!AVPixelFormat;
    const int planes = av_pix_fmt_count_planes(format);
    if (planes < 0)
    {
        throw new Exception("target format is not a valid pixel format");
    }
    for (int i = 0; i < planes; i++)
    {
        if (!target.data[i] ||
            !source.data[i])
        {
            throw new Exception("target or source data is empty");
        }
    }

    av_image_copy(target.data[0 .. 4], target.linesize[0 .. 4],
                  source.data[0 .. 4], source.linesize[0 .. 4],
                  format, source.width, source.height);
}

void copyVideo(in Frame source,
               ref Frame target)
{
    copyVideo(source.avFrame,
              target.avFrame);
}
alias copyPixels = copyVideo;

// #define CHECK_CHANNELS_CONSISTENCY(frame)                               \
//     av_assert2(!(frame)->channel_layout ||                              \
//                (frame)->channels ==                                     \
//                av_get_channel_layout_nb_channels((frame)->channel_layout))

void copyAudio(const AVFrame *source,
               AVFrame *target)
{
    const format = target.format.to!AVSampleFormat;
    const int planar = av_sample_fmt_is_planar(format);
    const int channels = target.channels;
    const int planes = planar ? channels : 1;

    if (target.nb_samples     != source.nb_samples ||
        target.channels       != source.channels ||
        target.channel_layout != source.channel_layout)
    {
        throw new Exception("mismatch");
    }

    // CHECK_CHANNELS_CONSISTENCY(source);

    for (int i = 0; i < planes; i++)
    {
        if (!target.extended_data[i] ||
            !source.extended_data[i])
        {
            throw new Exception("mismatch");
        }
    }

    av_samples_copy(target.extended_data,
                    cast(const(uint**))source.extended_data,
                    0, 0,
                    target.nb_samples, channels, format);
}

void copyAudio(in Frame source,
               ref Frame target)
{
    copyAudio(source.avFrame,
              target.avFrame);
}

alias copySamples = copyAudio;

/** Frame. */
struct Frame
{
    this(AVFrame* avFrame, bool finished = false)
    {
        if (avFrame)
        {
            this._avFrame = avFrame;
        }
        else
        {
            this._avFrame = av_frame_alloc();
        }
        this._finished = finished;
        assert(this._avFrame);
        // av_frame_ref(this._avFrame, avFrame);
    }

    this(this) @nogc                  // postblit
    {
        if (_avFrame.buf[0]) /* From AVFrame doc: An AVFrame is considered to be
                              * reference counted if at least one reference is
                              * set, i.e. if AVFrame.buf[0] != NULL */
        {
            _avFrame = av_frame_clone(_avFrame);
        }
        else
        {
            _avFrame = av_frame_alloc();
        }
    }

    ~this()
    {
        // av_frame_unref(_avFrame); // TODO this segfaults
        if (_avFrame)
            av_frame_free(&_avFrame);
    }

    void initFromAVFrame(AVFrame *avFrame)
    {
        av_frame_copy(_avFrame, avFrame);
        av_frame_copy_props(_avFrame, avFrame);
    }

    void opAssign(Frame frame)
    {
        initFromAVFrame(frame._avFrame);
    }

    @safe @nogc pure nothrow:

    bool finished() const { return _finished; }

    inout(AVFrame*) avFrame() inout { return _avFrame; }

    // alias _avFrame this;
private:
    AVFrame* _avFrame = null;
    bool _finished = false;
}

unittest
{
    auto frame1 = Frame(null);   // this
    auto frame2 = frame1;        // this(this)
    auto frame3 = frame2;       // this(this)
    frame3 = frame2;            // opAssign
}

/** By Packet InputRange. */
private struct ByPacket
{
    this(ref FormatContext formatContext)
    {
        writeln("ByPacket.this(ref Format format) entered");
        this._formatContext = formatContext; // make a new fresh copy to get functional semantics on range
        writeln("x");
        _index = 0;
        next;
    }
    void popFront() @safe { next; }
    bool empty() const @safe @nogc pure nothrow { return _empty; }
    auto ref front() const @safe @nogc pure nothrow { return _packet; }
    size_t index() const @safe @nogc pure nothrow { return _index; }
private:
    void next() @safe
    {
        _empty = av_read_frame(_formatContext._avFormatContext, &_packet) < 0;
        if (!_empty) ++_index;
    }

    FormatContext _formatContext;
    AVPacket _packet;
    bool _empty;
    size_t _index; // zero-based frame index
}

/** Wraps $(D avcodec_decode_video2) because we trust it to not leak $(D got_picture). */
Tuple!(int, bool) avcodec_decode_video2_d(AVCodecContext *avctx, AVFrame *picture,
                                          const AVPacket *avpkt) @trusted
{
    int got_picture;
    int ret = avcodec_decode_video2(avctx, picture, &got_picture, avpkt);
    return  tuple(ret, got_picture != 0);
}

/** By Frame InputRange. */
private struct ByFrame
{
    @disable this();

    this(ref FormatContext formatContext,
         AVStream* avStream,
         AVFrame *avFrame)
    {
        this._byPacket = ByPacket(formatContext);
        assert(!_byPacket.empty);
        this._avStream = avStream;
        this._frame = Frame(avFrame);
        if (!empty) popFront; // try get first
    }
    bool empty() const @safe @nogc pure nothrow { return _byPacket.empty; }
    auto ref front() @nogc nothrow { return _frame; }
    void popFront() @safe
    {
        while (true)
        {
            _byPacket.popFront;
            const ret = avcodec_decode_video2_d(_avStream.codec,
                                                _frame.avFrame,
                                                &(_byPacket.front()));
            if (ret[0] < 0)
            {
                throw new Exception("avcodec_decode_video2: returnerd: ", ret[0].to!string);
            }
            if (ret[1]) break;
        }
    }
private:
    ByPacket _byPacket;
    AVStream* _avStream = null;
    Frame _frame;
}

struct Stream
{
    AVMediaType mediaType() const @safe @nogc pure nothrow { return avStream.codec.codec_type; }

    @disable this();

    this(ref FormatContext formatContext,
         AVStream* avStream)
    in
    {
        assert(formatContext._avFormatContext);
        assert(avStream);
    }
    body
    {
        this._formatContext = &formatContext;
        this.avStream = avStream;
    }

    /** Iterate By $(D AVFrame). */
    auto byFrame(AVFrame *avFrame = null)
    {
        return ByFrame(*_formatContext, avStream, avFrame);
    }

    alias avStream this;
    AVStream* avStream;
private:
    FormatContext* _formatContext;
}

/** AV Format Context.
 */
struct FormatContext
{
public:
    @disable this();

    this(string path)
    {
        this.path = path;
        load(path);
    }

    ~this()
    {
        closeInput;
    }

    void load(string path)
    {
        this._avFormatContext = avformat_alloc_context();
        assert(this._avFormatContext);
        openInput(path);
        findStreamInfo(path);
    }

    /** Return: Streams contained in $(D path). */
    auto streams() nothrow // TODO @nogc
    {
        import std.algorithm.iteration: map;
        // See also: http://forum.dlang.org/post/jmijavvapsqtuyftfqvh@forum.dlang.org
        return _avFormatContext.streams[0 .. _avFormatContext.nb_streams]
                               .map!(avs => Stream(this, avs)); // WARNING: unsafe stuff!
    }

private:

    void openInput(const string path)
    {
        if (avformat_open_input(&_avFormatContext, path.toStringz, null, null) != 0) // open video file
        {
            throw new FileException(path, "avformat_open_input failed: Couldn't open file");
        }
    }

    void closeInput()
    {
        avformat_close_input(&_avFormatContext);
    }

    void findStreamInfo(const string path)
    {
        if (avformat_find_stream_info(_avFormatContext, null) < 0) // get stream information
        {
            throw new FileException(path, "avformat_find_stream_info failed: Couldn't find stream information");
        }
    }

    /** Dump Format Information. */
    void dumpFormat()
    {
        av_dump_format(_avFormatContext, 0, path.toStringz, 0);
    }

    string path;
    AVFormatContext* _avFormatContext = null;
}

/** AV Format (File).
 */
struct Format
{
public:
    @disable this();

    this(string path)
    {
        this.path = path;
    }

    this(this)
    {
        writeln("Format.this(this) entered");
    }

    /** Return: Streams contained in $(D path). */
    auto streams() // TODO @nogc
    {
        return FormatContext(path).streams;
    }

    ~this()
    {
        closeStreamsCodecs;
    }

    /** Iterate By $(D AVPacket). */
    auto byPacket()
    {
        auto formatContext = FormatContext(path);
        return ByPacket(formatContext);
    }

private:
    void closeStreamsCodecs()
    {
        foreach (stream; streams)
        {
            avcodec_close(stream.avStream.codec);
        }
    }

    const string path;
}

unittest
{
    import std.stdio: stderr;
    backtrace.backtrace.install(stderr);

    import std.algorithm.searching: count;
    import std.range.primitives: front;
    import std.stdio: writeln;

    const path = `/home/per/justd/av/test-data/ubuntu.mov`;
    auto format = Format(path); // TODO better URL lookup logic
    assert(format.path == path);

    // streams
    auto streams = format.streams;
    assert(streams.count == 1);

    writeln("a");

    const Stream stream = streams.front;
    writeln("b");

    assert(stream.avStream.index == 0);
    assert(stream.avStream.avstream_id == 1);
    assert(stream.avStream.duration == 145145);
    assert(stream.avStream.avg_frame_rate == AVRational(30000,
                                                        1001));
    // assert(stream.avStream.r_frame_rate == AVRational(-1326270491, -54677315));
    assert(stream.avStream.sample_aspect_ratio == AVRational(0,
                                                             1));


    const AVCodecContext* codecCtx = stream.avStream.codec;
    assert(codecCtx.codec_type == AVMediaType.AVMEDIA_TYPE_VIDEO);

    const AVCodec* codec = avcodec_find_decoder(codecCtx.codec_id);
    assert(codec.name.to!string == "mpeg4");
    assert(codec.long_name.to!string == "MPEG-4 part 2");
    assert(codec.type == AVMediaType.AVMEDIA_TYPE_VIDEO);
    assert(codec.id == AVCodecID.AV_CODEC_ID_MPEG4);

    // packets

    // check that byPacket is functional
    assert(format.byPacket.count == 144);
    assert(format.byPacket.count == 144);

    auto packets = format.byPacket;

    // first packet
    assert(packets.front.pts == 0);
    assert(packets.front.dts == 0);
    assert(packets.front.duration == 1001);
    assert(packets.front.size == 11375);
    assert(packets.front.stream_index == 0);
    assert(packets.front.pos == 36);

    packets.popFront;

    // second packet
    assert(packets.front.pts == 1001);
    assert(packets.front.dts == 1001);
    assert(packets.front.duration == 3003);
    assert(packets.front.size == 5867);
    assert(packets.front.stream_index == 0);
    assert(packets.front.pos == 11411);

    // decode frames
    size_t i;
    foreach (frame; format.streams.front.byFrame) // for each frame in stream
    {
        writeln("i: ", i);
        writeln("width: ", frame.avFrame.width);
        writeln("height: ", frame.avFrame.height);
        writeln("frame: ", frame.avFrame.tupleof);
        ++i;
    }

    writeln("sd");
}
